using System.Collections;
using System.Collections.Generic;
using Game.Config;
using Game.Enums;
using Game.Items;
using UnityEngine;

namespace Game
{
	public class ItemSpawner : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private float m_randomRange;
		[SerializeField] private AbstractItem m_itemPrefab;

		#endregion

		#region PrivateFields

		private Coroutine m_spawnCoroutine;
		private List<ESmellType> m_levelSmellList;
		private List<AbstractItem> m_spawnedItems;
		private LevelInfo m_levelInfo;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			m_spawnedItems = new List<AbstractItem>();
		}

		#endregion

		#region PublicMethods

		public void StartSpawning(LevelInfo levelInfo)
		{
			StopSpawning();

			m_levelInfo = levelInfo;
			m_spawnCoroutine = StartCoroutine(SpawnCoroutine());
		}

		public void StopSpawning()
		{
			if (m_spawnCoroutine != null)
			{
				StopCoroutine(m_spawnCoroutine);
			}
		}

		public void Cleanup()
		{
			foreach (AbstractItem item in m_spawnedItems)
			{
				if (item != null)
				{
					Destroy(item.gameObject);
				}
			}
			
			m_spawnedItems.Clear();
		}

		#endregion

		#region PrivateMethods

		private IEnumerator SpawnCoroutine()
		{
			SpawnItem();

			while (true)
			{
				yield return new WaitForSeconds(m_levelInfo.Gap / m_levelInfo.Speed);
				SpawnItem();
			}
		}

		private void SpawnItem()
		{
			AbstractItem item = Instantiate(m_itemPrefab);
			item.Initialize(m_levelInfo.SmellList[Random.Range(0, m_levelInfo.SmellList.Count)], Random.value > .5f);

			item.transform.SetParent(transform);
			item.StartMove(new Vector3(Random.Range(-m_randomRange, m_randomRange), 0, 0), m_levelInfo.Speed);

			m_spawnedItems.Add(item);
		}

		#endregion
	}
}
