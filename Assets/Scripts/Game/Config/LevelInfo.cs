using System.Collections.Generic;
using Game.Enums;
using UnityEngine;

namespace Game.Config
{
	[CreateAssetMenu(fileName = "Level", menuName = "Config/LevelInfo", order = 1)]
	public class LevelInfo : ScriptableObject
	{
		#region PublicFields

		public float Speed;
		public float Gap;
		public List<ESmellType> SmellList;

		#endregion
	}
}
