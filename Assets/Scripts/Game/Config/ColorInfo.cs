using System;
using System.Collections.Generic;
using System.Linq;
using Game.Enums;
using UnityEngine;

namespace Game.Config
{
	[CreateAssetMenu(fileName = "Colors", menuName = "Config/Colors", order = 1)]
	public class ColorInfo : ScriptableObject
	{
		#region PublicFields

		public List<SmellColor> SmellColors;

		#endregion

		#region PublicMethods

		public Color GetColor(ESmellType smellType)
		{
			return SmellColors.FirstOrDefault(c => c.SmellType == smellType).Color;
		}

		#endregion

		#region NestedTypes

		[Serializable]
		public struct SmellColor
		{
			public ESmellType SmellType;
			public Color Color;
		}

		#endregion
	}
}
