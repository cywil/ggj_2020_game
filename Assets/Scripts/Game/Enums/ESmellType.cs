namespace Game.Enums
{
	public enum ESmellType
	{
		None,
		Sock,
		Fish,
		Garbage,
		Mushrooms
	}
}
