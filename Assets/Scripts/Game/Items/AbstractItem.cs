using Game.Enums;
using UnityEngine;

namespace Game.Items
{
	public abstract class AbstractItem : MonoBehaviour
	{
		#region PublicFields

		public bool IsUsed { get; set; }
		public abstract bool IsActiveItem { set; }
		public abstract bool IsDynamic { set; get; }

		#endregion

		#region ProtectedFields

		protected Rigidbody2D m_rigidbody2D;
		protected bool m_isMoving;
		protected float m_speed;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			m_rigidbody2D = GetComponent<Rigidbody2D>();
			IsDynamic = false;
		}

		#endregion

		#region PublicMethods

		public abstract void Initialize(ESmellType smellType, bool isNegative = false);
		public abstract (ESmellType SmellType, float Value) GetModifier();
		public abstract void StartMove(Vector3 startPosition, float speed);
		public abstract void MoveToTarget(Vector3 targetPosition);

		#endregion
	}
}
