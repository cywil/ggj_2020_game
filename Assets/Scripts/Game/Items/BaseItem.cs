using System;
using System.Collections.Generic;
using DG.Tweening;
using Game.Enums;
using UnityEngine;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace Game.Items
{
	public class BaseItem : AbstractItem
	{
		#region PublicFields

		public override bool IsActiveItem
		{
			set => m_active.SetActive(value);
		}

		public override bool IsDynamic
		{
			set
			{
				m_rigidbody2D.isKinematic = !value;

				if (value)
				{
					m_arrowDown.SetActive(false);
					m_arrowUp.SetActive(false);
				}
			}

			get => !m_rigidbody2D.isKinematic;
		}

		public bool HitPlaying => m_hitSound.isPlaying;

		#endregion

		#region SerializeFields

		[SerializeField] private List<ItemView> m_items;
		[SerializeField] private SpriteRenderer m_bg;
		[SerializeField] private float m_modifierValue;
		[SerializeField] private GameObject m_active;
		[SerializeField] private GameObject m_arrowUp;
		[SerializeField] private GameObject m_arrowDown;
		[SerializeField] private AudioSource m_hitSound;

		#endregion

		#region PrivateFields

		private ESmellType m_smellType;
		private bool m_isNegative;
		private bool m_isUsed;
		private GameController m_gameController;

		#endregion

		#region UnityMethods

		private void OnCollisionEnter2D(Collision2D other)
		{
			if (HitPlaying)
			{
				return;
			}

			var otherItem = other.collider.GetComponent<BaseItem>();

			if (otherItem != null && otherItem.HitPlaying)
			{
				return;
			}

			m_hitSound.pitch = Random.Range(1.5f, 2f);
			m_hitSound.Play();
		}

		private void FixedUpdate()
		{
			if (!m_isMoving)
			{
				return;
			}

			m_rigidbody2D.velocity = Vector2.down * m_speed;
		}

		#endregion

		#region PublicMethods

		public override void Initialize(ESmellType smellType, bool isNegative = false)
		{
			m_gameController = GameController.Instance;
			IsActiveItem = false;

			m_isNegative = isNegative;
			m_smellType = smellType;

			UpdateView();
		}

		public override (ESmellType SmellType, float Value) GetModifier()
		{
			return (m_smellType, m_modifierValue * (m_isNegative ? -1 : 1));
		}

		public override void StartMove(Vector3 startPosition, float speed)
		{
			m_speed = speed;

			transform.localPosition = startPosition;
			m_isMoving = true;
		}

		public override void MoveToTarget(Vector3 targetPosition)
		{
			IsDynamic = false;
			transform.DOMove(targetPosition, .25f).SetEase(Ease.InSine).OnComplete(OnMoveToTargetCompleted);
		}

		#endregion

		#region PrivateMethods

		private void OnMoveToTargetCompleted()
		{
			Destroy(gameObject);
		}

		private void UpdateView()
		{
			m_arrowUp.gameObject.SetActive(!m_isNegative);
			m_arrowDown.gameObject.SetActive(m_isNegative);

			foreach (ItemView itemView in m_items)
			{
				itemView.View.SetActive(itemView.SmellType == m_smellType);
			}

			if (m_gameController != null)
			{
				m_bg.color = m_gameController.ColorInfo.GetColor(m_smellType);
			}
		}

		#endregion

		#region NestedTypes

		[Serializable]
		private struct ItemView
		{
			[FormerlySerializedAs("Type")] public ESmellType SmellType;
			public GameObject View;
		}

		#endregion
	}
}
