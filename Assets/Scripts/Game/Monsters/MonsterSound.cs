using System.Collections.Generic;
using UnityEngine;

namespace Game.Monsters
{
	public class MonsterSound : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private AudioSource m_eatSource;
		[SerializeField] private List<AudioClip> m_eatClips;

		#endregion

		#region PublicMethods

		public void PlayEat()
		{
			if (m_eatClips.Count == 0)
			{
				Debug.LogError("No eat sounds");
				return;
			}

			m_eatSource.pitch = Random.Range(.8f, 1.3f);
			m_eatSource.PlayOneShot(m_eatClips[Random.Range(0, m_eatClips.Count)]);
		}

		#endregion
	}
}
