using System.Collections.Generic;
using System.Text;
using Game.Effects;
using Game.Enums;
using Game.Items;
using Game.Stats;
using TMPro;
using UnityEngine;
using UnityEngine.Serialization;

namespace Game.Monsters
{
	public abstract class AbstractMonster : MonoBehaviour
	{
		#region PublicFields

		public Vector3 ItemTargetPosition => m_itemTarget.position;
		public SmellStats SmellStats { get; private set; }

		public int Direction
		{
			set
			{
				Vector3 scale = m_container.localScale;
				scale.x = Mathf.Abs(scale.x) * value;
				m_container.localScale = scale;
			}
		}

		#endregion

		#region SerializeFields

		[SerializeField] private TextMeshProUGUI m_debug;
		[SerializeField] private Transform m_container;
		[SerializeField] private Transform m_itemTarget;

		[FormerlySerializedAs("m_smellSmellStats")]
		[SerializeField]
		private SmellStatsView m_smellStats;

		[SerializeField] private SmellParticles m_smellParticles;
		[SerializeField] private MonsterSound m_monsterSound;

		#endregion

		#region PublicMethods

		public void Initialize(List<ESmellType> smellList, AbstractMonster otherMonster = null)
		{
			int attempt = 0;
			SmellStats = new SmellStats(smellList);

			while (otherMonster != null && SmellStats.Equals(otherMonster.SmellStats) && attempt < 20)
			{
				SmellStats = new SmellStats(smellList);
				attempt++;
			}

			SmellStats.OnStatsChanged += OnSmellStatsChanged;

			m_smellStats.Initialize(smellList);
			UpdateStatsView(SmellStats.SmellValues);
		}

		public void ApplyItem(AbstractItem item)
		{
			(ESmellType SmellType, float Value) modifier = item.GetModifier();
			SmellStats.AddValueToState(modifier.SmellType, modifier.Value);

			m_monsterSound.PlayEat();
		}

		#endregion

		#region PrivateMethods

		private void OnSmellStatsChanged(Dictionary<ESmellType, float> smell)
		{
			UpdateStatsView(smell);
		}

		private void UpdateStatsView(Dictionary<ESmellType, float> smell)
		{
			m_smellStats.UpdateStats(smell);
			m_smellParticles.UpdateParticles(smell);

			// Debug
			var stringBuilder = new StringBuilder();

			foreach (KeyValuePair<ESmellType, float> keyValuePair in smell)
			{
				stringBuilder.AppendLine($"{keyValuePair.Key}: {keyValuePair.Value:F2}");
			}

			m_debug.text = stringBuilder.ToString();
		}

		#endregion
	}
}
