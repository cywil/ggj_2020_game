using System;
using System.Collections.Generic;
using System.Linq;
using Game.Enums;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Game.Monsters
{
	public class SmellStats : IEquatable<SmellStats>
	{
		#region Constants

		private const float EQUAL_TOLERANCE = .08f;

		#endregion

		#region PublicFields

		public Dictionary<ESmellType, float> SmellValues { get; private set; }

		#endregion

		#region EventsAndDelegates

		public event Action<Dictionary<ESmellType, float>> OnStatsChanged;

		#endregion

		#region Constructors

		public SmellStats(List<ESmellType> smellList)
		{
			SmellValues = new Dictionary<ESmellType, float>();

			foreach (ESmellType smellType in smellList)
			{
				SmellValues[smellType] = Random.value;
			}

			NormalizeSmell();
			NotifyOnChanged();
		}

		#endregion

		#region InterfaceImplementations

		public bool Equals(SmellStats other)
		{
			foreach (KeyValuePair<ESmellType, float> pair in SmellValues)
			{
				if (!other.SmellValues.ContainsKey(pair.Key))
				{
					Debug.LogError("Stats have different set.");
					return false;
				}

				if (Mathf.Abs(pair.Value - other.SmellValues[pair.Key]) > EQUAL_TOLERANCE)
				{
					return false;
				}
			}

			return true;
		}

		#endregion

		#region PublicMethods

		public void AddValueToState(ESmellType smellType, float value)
		{
			bool hasStat = SmellValues.TryGetValue(smellType, out float currentValue);

			if (hasStat)
			{
				var tempSmellValues = new Dictionary<ESmellType, float>();

				foreach (KeyValuePair<ESmellType, float> keyValuePair in SmellValues)
				{
					tempSmellValues[keyValuePair.Key] = keyValuePair.Key == smellType
						? keyValuePair.Value + value
						: keyValuePair.Value - value / (SmellValues.Count - 1);

					tempSmellValues[keyValuePair.Key] = Mathf.Clamp01(tempSmellValues[keyValuePair.Key]);
				}

				SmellValues = tempSmellValues;
			}

			NormalizeSmell();
			NotifyOnChanged();
		}

		#endregion

		#region PrivateMethods

		private void NotifyOnChanged()
		{
			OnStatsChanged?.Invoke(SmellValues);
		}

		private void NormalizeSmell()
		{
			float total = 0;
			SmellValues.Values.ToList().ForEach(v => total += v);

			var normalized = new Dictionary<ESmellType, float>();

			foreach (KeyValuePair<ESmellType, float> smellType in SmellValues)
			{
				normalized[smellType.Key] = smellType.Value / total;
			}

			SmellValues = normalized;
		}

		#endregion
	}
}
