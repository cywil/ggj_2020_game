using Game.Items;
using UnityEngine;

namespace Game
{
	public class ItemCatcher : MonoBehaviour
	{
		#region PrivateFields

		private AbstractItem m_currentItem;

		#endregion

		#region UnityMethods

		private void OnTriggerEnter2D(Collider2D other)
		{
			var item = other.gameObject.GetComponent<AbstractItem>();

			if (item != null && !item.IsDynamic)
			{
				m_currentItem = item;
				m_currentItem.IsActiveItem = true;
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{
			var item = other.gameObject.GetComponent<AbstractItem>();

			if (item != null)
			{
				if (m_currentItem == item)
				{
					m_currentItem = null;
				}

				item.IsActiveItem = false;
				item.IsDynamic = true;
			}
		}

		#endregion

		#region PublicMethods

		public AbstractItem GetItem()
		{
			return m_currentItem;
		}

		#endregion
	}
}
