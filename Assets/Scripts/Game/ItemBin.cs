using System;
using System.Collections.Generic;
using Game.Items;
using UnityEngine;

namespace Game
{
	public class ItemBin : MonoBehaviour
	{
		#region EventsAndDelegates

		public event Action OnBinFilled;

		#endregion

		#region SerializeFields

		[SerializeField] private int m_capacity;

		#endregion

		#region PrivateFields

		private List<AbstractItem> m_items;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			m_items = new List<AbstractItem>();
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			var item = other.gameObject.GetComponent<AbstractItem>();

			if (item != null && item.IsDynamic)
			{
				m_items.Add(item);
			}

			if (m_items.Count >= m_capacity)
			{
				OnBinFilled?.Invoke();
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{
			var item = other.gameObject.GetComponent<AbstractItem>();

			if (item != null && m_items.Contains(item))
			{
				m_items.Remove(item);
			}
		}

		#endregion

		public void Cleanup()
		{
			m_items.Clear();
		}
	}
}
