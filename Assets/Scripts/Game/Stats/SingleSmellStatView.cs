using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Game.Enums;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game.Stats
{
	public class SingleSmellStatView : MonoBehaviour
	{
		#region PublicFields

		public float Value
		{
			set => m_bar.DOFillAmount(value, m_animTime);
		}

		#endregion

		#region SerializeFields

		[SerializeField] private TextMeshProUGUI m_nameLabel;
		[SerializeField] private Image m_bar;
		[SerializeField] private Image m_barBg;
		[SerializeField] private Image m_circle;
		[SerializeField] private Image m_icon;
		[SerializeField] private float m_animTime;
		[Header("Colors")] [SerializeField] private List<StatIcon> m_icons;

		#endregion

		#region PrivateFields

		private GameController m_gameController;

		#endregion

		#region PublicMethods

		public void Initialize(ESmellType smellType)
		{
			m_gameController = GameController.Instance;
			m_nameLabel.text = smellType.ToString();

			if (m_gameController != null)
			{
				m_bar.color = m_gameController.ColorInfo.GetColor(smellType);
				m_barBg.color = m_bar.color;
				m_circle.color = m_bar.color;
			}

			m_icon.sprite = m_icons.FirstOrDefault(s => s.Smell == smellType).Icon;
			m_icon.SetNativeSize();
		}

		#endregion

		#region NestedTypes

		[Serializable]
		private struct StatIcon
		{
			public ESmellType Smell;
			public Sprite Icon;
		}

		#endregion
	}
}
