using System.Collections.Generic;
using Game.Enums;
using UnityEngine;

namespace Game.Stats
{
	public class SmellStatsView : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private RectTransform m_smellStatContainer;
		[SerializeField] private SingleSmellStatView m_smellStatPrefab;

		#endregion

		#region PrivateFields

		private Dictionary<ESmellType, SingleSmellStatView> m_smellStatViews;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			m_smellStatViews = new Dictionary<ESmellType, SingleSmellStatView>();
		}

		#endregion

		#region PublicMethods

		public void Initialize(List<ESmellType> smellList)
		{
			foreach (ESmellType smell in smellList)
			{
				SingleSmellStatView smellStatView = Instantiate(m_smellStatPrefab);
				m_smellStatViews[smell] = smellStatView;
				smellStatView.Initialize(smell);

				smellStatView.transform.SetParent(m_smellStatContainer, false);
				smellStatView.gameObject.SetActive(true);
			}
		}

		public void UpdateStats(Dictionary<ESmellType, float> smellList)
		{
			foreach (KeyValuePair<ESmellType, float> smellStat in smellList)
			{
				m_smellStatViews.TryGetValue(smellStat.Key, out SingleSmellStatView statView);
				{
					statView.Value = smellStat.Value;
				}
			}
		}

		#endregion
	}
}
