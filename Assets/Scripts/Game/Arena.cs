using System.Collections.Generic;
using Game.Config;
using Game.Monsters;
using UnityEngine;

namespace Game
{
	public class Arena : MonoBehaviour
	{
		#region PublicFields

		public AbstractMonster MonsterA { get; private set; }
		public AbstractMonster MonsterB { get; private set; }

		#endregion

		#region SerializeFields

		[SerializeField] private List<AbstractMonster> m_monsterPrefabs;
		[SerializeField] private Transform m_containerA;
		[SerializeField] private Transform m_containerB;

		#endregion

		#region PublicMethods

		public void CreateMonsters(LevelInfo levelInfo)
		{
			if (m_monsterPrefabs.Count == 0)
			{
				Debug.LogError("No monster found.");
				return;
			}

			MonsterA = Instantiate(m_monsterPrefabs[Random.Range(0, m_monsterPrefabs.Count)]);
			MonsterB = Instantiate(m_monsterPrefabs[Random.Range(0, m_monsterPrefabs.Count)]);

			MonsterA.transform.SetParent(m_containerA);
			MonsterB.transform.SetParent(m_containerB);

			MonsterA.transform.localPosition = Vector3.zero;
			MonsterB.transform.localPosition = Vector3.zero;

			MonsterB.Direction = -1;

			MonsterA.Initialize(levelInfo.SmellList);
			MonsterB.Initialize(levelInfo.SmellList, MonsterA);
		}

		public void Cleanup()
		{
			if (MonsterA != null)
			{
				Destroy(MonsterA.gameObject);
			}

			if (MonsterB != null)
			{
				Destroy(MonsterB.gameObject);
			}
		}

		#endregion
	}
}
