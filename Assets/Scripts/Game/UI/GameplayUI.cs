using UnityEngine;

namespace Game.UI
{
	public class GameplayUI : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private GameObject m_won;
		[SerializeField] private GameObject m_nextLevel;
		[SerializeField] private GameObject m_youLost;

		#endregion

		#region PublicMethods

		public void HideAll()
		{
			m_won.SetActive(false);
			m_youLost.SetActive(false);
			m_nextLevel.SetActive(false);
		}

		public void ShowWon()
		{
			m_won.SetActive(true);
			m_youLost.SetActive(false);
			m_nextLevel.SetActive(false);
		}

		public void ShowLost()
		{
			m_won.SetActive(false);
			m_youLost.SetActive(true);
			m_nextLevel.SetActive(false);
		}

		public void ShowNext()
		{
			m_won.SetActive(false);
			m_youLost.SetActive(false);
			m_nextLevel.SetActive(true);
		}

		#endregion
	}
}
