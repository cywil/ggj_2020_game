using UnityEngine;

namespace Game.UI
{
	public class IntroScreen : MonoBehaviour
	{
		#region PrivateFields

		private GameController m_gameController;

		#endregion

		#region UnityMethods

		private void Start()
		{
			m_gameController = GameController.Instance;
		}

		private void Update()
		{
			if (Input.anyKey && m_gameController != null)
			{
				m_gameController.StartGame();
			}
		}

		#endregion

		#region PrivateMethods

		public void Hide()
		{
			gameObject.SetActive(false);
		}

		public void Show()
		{
			gameObject.SetActive(true);
		}

		#endregion
	}
}
