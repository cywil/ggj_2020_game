using System;
using System.Collections.Generic;
using Game.Enums;
using UnityEngine;

namespace Game.Effects
{
	public class SmellParticles : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private float m_maxEmission;
		[SerializeField] private List<SmellParticleSystem> m_smellParticles;

		#endregion

		#region PublicMethods

		public void UpdateParticles(Dictionary<ESmellType, float> values)
		{
			foreach (SmellParticleSystem particle in m_smellParticles)
			{
				ParticleSystem.EmissionModule emission = particle.Particles.emission;

				emission.rateOverTime = values.ContainsKey(particle.SmellType)
					? values[particle.SmellType] * m_maxEmission
					: 0;
			}
		}

		#endregion

		#region NestedTypes

		[Serializable]
		private struct SmellParticleSystem
		{
			public ESmellType SmellType;
			public ParticleSystem Particles;
		}

		#endregion
	}
}
