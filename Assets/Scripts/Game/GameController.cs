using System.Collections;
using System.Collections.Generic;
using Game.Config;
using Game.Items;
using Game.Monsters;
using Game.UI;
using UnityEngine;
using UnityEngine.Serialization;
using Utils;

namespace Game
{
	public class GameController : Singleton<GameController>
	{
		#region PublicFields

		public ColorInfo ColorInfo => m_colorInfo;

		#endregion

		#region SerializeFields

		[FormerlySerializedAs("m_levelInfo")]
		[SerializeField]
		private List<LevelInfo> m_levels;

		[SerializeField] private IntroScreen m_intro;
		[SerializeField] private GameplayUI m_gameUI;
		[SerializeField] private ColorInfo m_colorInfo;
		[SerializeField] private Arena m_arena;
		[SerializeField] private ItemSpawner m_itemSpawner;
		[SerializeField] private ItemCatcher m_itemCatcher;
		[SerializeField] private ItemBin m_itemBin;

		#endregion

		#region PrivateFields

		private int m_currentLevel;
		private bool m_gameEnd;
		private bool m_gameIsPlaying;

		#endregion

		#region UnityMethods

		private void Start()
		{
			m_intro.Show();
			m_gameUI.HideAll();
		}

		private void Update()
		{
			ProcessInput();
		}

		#endregion

		#region PublicMethods

		public void StartGame()
		{
			CleanLevel();
			m_intro.Hide();

			m_gameEnd = false;

			m_currentLevel = 0;
			StartLevel(m_currentLevel);
		}

		#endregion

		#region PrivateMethods

		private void StartLevel(int number)
		{
			m_gameIsPlaying = true;

			m_itemBin.OnBinFilled += OnBinFilled;

			m_gameUI.HideAll();
			m_arena.CreateMonsters(m_levels[number]);
			m_itemSpawner.StartSpawning(m_levels[number]);
		}

		private void CleanLevel()
		{
			m_arena.Cleanup();
			m_itemBin.Cleanup();
			m_itemSpawner.Cleanup();
		}

		private void OnBinFilled()
		{
			EndGame(false);
		}

		private void EndGame(bool isWin)
		{
			StopAllCoroutines();

			if (isWin)
			{
				m_gameUI.ShowWon();
			}
			else
			{
				m_gameUI.ShowLost();
			}

			m_gameEnd = true;
			m_itemBin.OnBinFilled -= OnBinFilled;
			m_itemSpawner.StopSpawning();

			Debug.Log(isWin ? "YOU WON" : "YOU LOST");

			StartCoroutine(ShowIntro());
		}

		private IEnumerator ShowIntro()
		{
			yield return new WaitForSeconds(2);
			
			CleanLevel();
			m_intro.Show();
		}

		private IEnumerator NextLevel()
		{
			m_gameIsPlaying = false;

			m_itemBin.OnBinFilled -= OnBinFilled;
			m_itemSpawner.StopSpawning();

			bool hasNextLevel = ++m_currentLevel < m_levels.Count;

			if (hasNextLevel)
			{
				m_gameUI.ShowNext();
			}
			else
			{
				m_gameUI.ShowWon();
			}

			yield return new WaitForSeconds(2);

			if (!hasNextLevel)
			{
				EndGame(true);
				yield break;
			}

			CleanLevel();
			StartLevel(m_currentLevel);
		}

		private void ProcessInput()
		{
			if (m_gameEnd || !m_gameIsPlaying)
			{
				return;
			}

			if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
			{
				AssignItemTo(m_arena.MonsterA);
			}

			if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
			{
				AssignItemTo(m_arena.MonsterB);
			}
		}

		private void AssignItemTo(AbstractMonster monster)
		{
			AbstractItem currentItem = m_itemCatcher.GetItem();

			if (currentItem == null || currentItem.IsUsed)
			{
				return;
			}

			monster.ApplyItem(currentItem);
			currentItem.IsUsed = true;
			currentItem.MoveToTarget(monster.ItemTargetPosition);

			CheckWinCondition();
		}

		private void CheckWinCondition()
		{
			if (m_arena.MonsterA.SmellStats.Equals(m_arena.MonsterB.SmellStats))
			{
				StartCoroutine(NextLevel());
			}
		}

		#endregion
	}
}
