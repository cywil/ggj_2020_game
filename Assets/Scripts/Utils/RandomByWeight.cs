using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

namespace Utils
{
	public class RandomByWeight : MonoBehaviour
	{
		#region SerializeFields

		[SerializeField] private List<StateWeight> m_results;

		#endregion

		#region UnityMethods

		private void Awake()
		{
			NormalizeWeights();
		}

		#endregion

		#region PublicMethods

		public int GetRandom()
		{
			float random = Random.value;
			float cumulative = 0f;

			for (int i = 0; i < m_results.Count; i++)
			{
				cumulative += m_results[i].Weight;

				if (random < cumulative)
				{
					return m_results[i].Result;
				}
			}

			return 0;
		}

		#endregion

		#region PrivateMethods

		private void NormalizeWeights()
		{
			m_results = m_results.OrderBy(w => w.Weight).ToList();

			float total = 0;
			m_results.ForEach(r => total += r.Weight);

			if (total > 0)
			{
				for (int i = 0; i < m_results.Count; i++)
				{
					m_results[i] = new StateWeight { Result = m_results[i].Result, Weight = m_results[i].Weight * 1 / total };
				}

				m_results.ForEach(r => Debug.Log(r.Weight));
			}
		}

		#endregion

		#region NestedTypes

		[Serializable]
		private struct StateWeight
		{
			public int Result;
			[Range(0, 1)] public float Weight;
		}

		#endregion
	}
}
